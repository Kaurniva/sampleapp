﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using BANKSAMPLEAPP.Model;
using Result = BANKSAMPLEAPP.Model.Result;

namespace BANKSAMPLEAPP
{
    [Activity(Label = "DirectionCardRecycleview")]



    public class RecycleViewHolderDirection : RecyclerView.ViewHolder
    {
        public ImageView atmImage { get; set; }
        public TextView name { get; set; }
        public TextView address { get; set; }



        private View view;

        public RecycleViewHolderDirection(View itemview) : base(itemview)
        {
            view = itemview;
            atmImage = itemview.FindViewById<ImageView>(Resource.Id.atmImage1);
            name = itemview.FindViewById<TextView>(Resource.Id.bankname);
            address = itemview.FindViewById<TextView>(Resource.Id.bankAddress);



        }
    }
    public class DirectionCardRecycleview : RecyclerView.Adapter
    {


        public List<Result> result = new List<Result>();
        public OnItemClick OnItem;
        private Result result1;

        public DirectionCardRecycleview(List<Result> result)
        {
            this.result = result;

        }

        public DirectionCardRecycleview(Result result1)
        {
            this.result1 = result1;
        }

        public void SetOnItemClick(OnItemClick itemClick)
        {
            this.OnItem = itemClick;
        }

        public override int ItemCount
        {
            get { return result.Count(); }
        }
        //  public override void OnBindViewHolder(RecycleViewHolderDirection.ViewHolder holder, int position)




        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            Result res = result[position];
            RecycleViewHolderDirection vh = holder as RecycleViewHolderDirection;
            vh.name.Text = res.name;
            vh.address.Text = res.vicinity;

            /*  string resultString = Intent.GetStringExtra(Utility.RESULT_ARG);
              if (result != null && resultString.Length > 0)
              {
                  Result result = JsonConvert.DeserializeObject<Result>(resultString);
                  place_id = result.place_id;
                  name = result.name;*/



            //string resultString = .PutExtra(Helper.Utility.RESULT_ARG, JsonConvert.De(result));



            if (res.isBankItem)
            {
                vh.atmImage.SetBackgroundResource(Resource.Drawable.bank_icn);
            }
            else
            {
                vh.atmImage.SetBackgroundResource(Resource.Drawable.atm_icn);
            }


        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.atm_card_details, parent, false);
            RecycleViewHolderDirection vh = new RecycleViewHolderDirection(itemView);
            return vh;


        }


    }


}


/*public class DirectionRecycleViewHolder : RecyclerView.ViewHolder
{
    public ImageView diectionImage { get; set; }
    public TextView routeWaydes { get; set; }
    public TextView totalDistance { get; set; }



    public DirectionRecycleViewHolder(View itemview) : base(itemview)
    {
        diectionImage = itemview.FindViewById<ImageView>(Resource.Id.imageView1);
        routeWaydes = itemview.FindViewById<TextView>(Resource.Id.textView1);
        totalDistance = itemview.FindViewById<TextView>(Resource.Id.totalDistance);

        // itemview.Click += (sender, e) => listener(base.Position);
    }
}

public class DirectionRecycleview : RecyclerView.Adapter
{

    public List<Leg> route = new List<Leg>();
    public OnItemClick OnItem;

    public DirectionRecycleview(List<Leg> route)
    {
        this.route = route;

    }

    public override int ItemCount
    {
        get { return route.Count(); }
    }

    public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        Leg leg = route[position];
        DirectionRecycleViewHolder vh = holder as DirectionRecycleViewHolder;
        // Route routeValue = route[position];

        vh.routeWaydes.Text = leg.start_address;
        vh.totalDistance.Text = leg.distance.text;
    }

    public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.direction_recycleview, parent, false);
        DirectionRecycleViewHolder vh = new DirectionRecycleViewHolder(itemView);
        return vh;
    }
}*/
