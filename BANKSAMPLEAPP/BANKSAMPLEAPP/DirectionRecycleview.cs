﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using System.Linq;
using BANKSAMPLEAPP.Model;
using BANKSAMPLEAPP.Helper;

namespace BANKSAMPLEAPP
{

    public class DirectionRecycleViewHolder : RecyclerView.ViewHolder
    {
        public ImageView diectionImage { get; set; }
        public TextView routeWaydes { get; set; }
        public TextView totalDistance { get; set; }



        public DirectionRecycleViewHolder(View itemview) : base(itemview)
        {
            diectionImage = itemview.FindViewById<ImageView>(Resource.Id.imageView1);
            routeWaydes = itemview.FindViewById<TextView>(Resource.Id.textView1);
            totalDistance = itemview.FindViewById<TextView>(Resource.Id.totalDistance);

            // itemview.Click += (sender, e) => listener(base.Position);
        }
    }

    public class DirectionRecycleview : RecyclerView.Adapter
    {

        public List<Step> route = new List<Step>();
        public OnItemClick OnItem;

        public DirectionRecycleview(List<Step> route)
        {
            this.route = route;

        }

        public override int ItemCount
        {
            get { return route.Count(); }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            Step leg = route[position];
            DirectionRecycleViewHolder vh = holder as DirectionRecycleViewHolder;
            // Route routeValue = route[position];

            vh.routeWaydes.Text = Utility.GetInstance.StripHTML(leg.html_instructions);
            vh.totalDistance.Text = leg.distance.text;
            if (leg.maneuver != null)
            {
                if (leg.maneuver.Equals(Utility.TURN_LEFT_KEY, StringComparison.OrdinalIgnoreCase))
                {
                    vh.diectionImage.SetImageResource(Resource.Drawable.arrow_left_icn);
                }
                else if (leg.maneuver.Equals(Utility.TURN_RIGHT_KEY, StringComparison.OrdinalIgnoreCase))
                {
                    vh.diectionImage.SetImageResource(Resource.Drawable.arrow_right_icn);
                }
                else
                {
                    vh.diectionImage.SetImageResource(Resource.Drawable.arrow1_icn);
                }
            }
            else
            {
                vh.diectionImage.SetImageResource(Resource.Drawable.arrow1_icn);
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.direction_recycleview, parent, false);
            DirectionRecycleViewHolder vh = new DirectionRecycleViewHolder(itemView);
            return vh;
        }
    }
}