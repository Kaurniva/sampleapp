﻿
using System.Threading.Tasks;
using BANKSAMPLEAPP.Model;
using BANKSAMPLEAPP.Request;
using Refit;

namespace BANKSAMPLEAPP.APIInterface
{
    [Headers("User-Agent: :request:")]
    public interface GoogleAPIInterface
    {
        [Get("/place/nearbysearch/json")]
        Task<string> GetPlaceList(GooglePlaceApiRequest googlePlaceApiRequest);

        [Get("/place/findplacefromtext/json")]
        Task<string> GetPlaceListFromText (GoogleFindPlaceFromTextRequest googlePlaceApiRequest);

        [Get("/directions/json")]
        Task<string> GetDirectionList(GoogleApiDirectionRequest googleApiDirectionRequest);
    }
}
