﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.Preferences;
using Android.Util;
using Android.Views.InputMethods;
using Android.Widget;
using BANKSAMPLEAPP.Model;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Helper
{
    public class Utility
    {
        private static readonly string MAP_API_KEY = "com.google.android.maps.v2.API_KEY";
        private static readonly string GOOGLE_PLACE_RESPONSE = "GooglePlaceResponse.txt";

        public static readonly string TEXTQUERY = "textquery";
        public static readonly string FORMATTED_ADDRESS = "formatted_address";
        public static readonly string NAME = "name";
        public static readonly string GEOMETRY = "geometry";
        public static readonly string TYPE = "type";
        public static readonly string RESULT_ARG = "resultArg";
        public static readonly string BUNDEL_ARG = "bundleArg";
        public static readonly string PLACE_ID_REQUEST = "place_id:";
        public static readonly string LAST_KNOWN_LOCATION_LAT_KEY = "LastKnownLocationLat";
        public static readonly string LAST_KNOWN_LOCATION_LNG_KEY = "LastKnownLocationLng";
        public static readonly string LAST_KNOWN_LOCATION_DETAILS_KEY = "LastKnownLocationDetails";
        public static readonly string WALKING_KEY = "walking";
        public static readonly string TURN_LEFT_KEY = "turn-left";
        public static readonly string TURN_RIGHT_KEY = "turn-right";
        private static readonly Utility Instance = new Utility();
        private Location location;

        private Utility()
        {
        }

        public static Utility GetInstance
        {
            get
            {
                return Instance;
            }
        }

        public string GetGoogleApiMetaData(Context context)
        {
            Android.OS.Bundle bundle = context.PackageManager.GetApplicationInfo(context.PackageName, PackageInfoFlags.MetaData).MetaData;
            String mayKey = "";
            foreach (var key in bundle.KeySet())
            {
                Log.Debug("SO", $"{key} : {bundle.GetString(key)}");
                if (key.Equals(MAP_API_KEY))
                {
                    mayKey = bundle.GetString(key);
                }
            }
            return mayKey;
        }

        public string GetFileString(Context context, string fileName)
        {
            AssetManager assets = context.Assets;
            string content;
            using (StreamReader sr = new StreamReader(assets.Open(fileName)))
            {
                content = sr.ReadToEnd();
            }
            return content;
        }

        public GooglePlaceResponse FindLocalGooglePlaceResponse(Context context)
        {
            return JsonConvert.DeserializeObject<GooglePlaceResponse>(GetFileString(context, GOOGLE_PLACE_RESPONSE));
        }

        public void DismissKeyboard(Context context, EditText editText)
        {
            var view = editText;
            if (view != null)
            {
                var imm = (InputMethodManager)context.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(view.WindowToken, 0);
            }
        }

        public void ShowToast(Context context, string msg)
        {
            Toast.MakeText(context, msg, ToastLength.Long).Show();
        }

        public Android.Locations.Location currentLocation { get; set; }

        public string ConvertStringToUrlEncoder(string value)
        {
            return System.Net.WebUtility.UrlEncode(value);
        }

        public void SaveLastLocationLat(Context context, double lat)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString(LAST_KNOWN_LOCATION_LAT_KEY, lat.ToString());
            editor.Apply();
        }

        public void SaveLastLocationLng(Context context, double lng)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString(LAST_KNOWN_LOCATION_LNG_KEY, lng.ToString());
            editor.Apply();
        }

        public string GetLastKnownLocationLat(Context context)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            return prefs.GetString(LAST_KNOWN_LOCATION_LAT_KEY, null);
        }

        public string GetLastKnownLocationLng(Context context)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            return prefs.GetString(LAST_KNOWN_LOCATION_LNG_KEY, null);
        }

        public string GetLastKnownLocationDetails(Context context)
        {
            string lat = GetLastKnownLocationLat(context);
            string lng = GetLastKnownLocationLng(context);
            if (lat != null && lng != null)
            {
                return lat + "," + lng;
            }
            else
            {
                return null;
            }

        }

        public string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
    }
}
