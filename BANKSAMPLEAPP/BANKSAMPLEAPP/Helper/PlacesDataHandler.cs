﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BANKSAMPLEAPP.Model;
using Java.Util;

namespace BANKSAMPLEAPP.Helper
{
    class PlacesDataHandler
    {
        private static readonly PlacesDataHandler Instance = new PlacesDataHandler();
        //<String, GoogleDirectionResponse>
        private HashMap hasMap;
        /**
         * Default constructor should private
         */
        private PlacesDataHandler()
        {
            hasMap = new HashMap();
        }

        public static PlacesDataHandler GetInstance()
        {
            return Instance;
        }

        public void UpdatePlaceDirectionDetails(string placeID, GoogleDirectionResponse googleDirectionResponse) => hasMap.Put(placeID, googleDirectionResponse);

        public GoogleDirectionResponse GetPlaceDirectionDetails(string placeID)
        {
            if (placeID != null)
            {
                return (GoogleDirectionResponse)hasMap.Get(placeID);
            }
            return null;
        }

        public bool IsPlaceDirectionDetailsAvailable(string placeID)
        {
            GoogleDirectionResponse googleDirectionResponse = GetPlaceDirectionDetails(placeID);
            return googleDirectionResponse != null;
        }
    }
}