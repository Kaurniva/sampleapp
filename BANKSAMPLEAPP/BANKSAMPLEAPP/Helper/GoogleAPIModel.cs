﻿using System;
using System.Net.Http;
using BANKSAMPLEAPP.APIInterface;
using BANKSAMPLEAPP.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Refit;

namespace BANKSAMPLEAPP
{
    public sealed class GoogleAPIModel
    {
        private static readonly string BASE_URL = "https://maps.googleapis.com/maps/api";
        private GoogleAPIInterface googleAPIInterface;

        private static readonly GoogleAPIModel Instance = new GoogleAPIModel();

        public GoogleAPIModel()
        {
            var httpClient = new HttpClient(new HttpLoggingHandler()) { BaseAddress = new System.Uri(BASE_URL) };
            googleAPIInterface = RestService.For<GoogleAPIInterface>(httpClient);
        }

        public static GoogleAPIModel GetInstance
        {
            get
            {
                return Instance;
            }
        }

        public GoogleAPIInterface GoogleAPIInterface
        {
            get
            {
                return googleAPIInterface;
            }
        }

    }
}
