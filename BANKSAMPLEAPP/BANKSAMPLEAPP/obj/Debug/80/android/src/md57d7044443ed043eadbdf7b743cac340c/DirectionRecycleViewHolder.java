package md57d7044443ed043eadbdf7b743cac340c;


public class DirectionRecycleViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("BANKSAMPLEAPP.DirectionRecycleViewHolder, BANKSAMPLEAPP", DirectionRecycleViewHolder.class, __md_methods);
	}


	public DirectionRecycleViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == DirectionRecycleViewHolder.class)
			mono.android.TypeManager.Activate ("BANKSAMPLEAPP.DirectionRecycleViewHolder, BANKSAMPLEAPP", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
