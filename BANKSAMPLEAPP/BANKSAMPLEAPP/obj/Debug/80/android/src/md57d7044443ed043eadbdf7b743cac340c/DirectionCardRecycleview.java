package md57d7044443ed043eadbdf7b743cac340c;


public class DirectionCardRecycleview
	extends android.support.v7.widget.RecyclerView.Adapter
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_getItemCount:()I:GetGetItemCountHandler\n" +
			"n_onBindViewHolder:(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V:GetOnBindViewHolder_Landroid_support_v7_widget_RecyclerView_ViewHolder_IHandler\n" +
			"n_onCreateViewHolder:(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;:GetOnCreateViewHolder_Landroid_view_ViewGroup_IHandler\n" +
			"";
		mono.android.Runtime.register ("BANKSAMPLEAPP.DirectionCardRecycleview, BANKSAMPLEAPP", DirectionCardRecycleview.class, __md_methods);
	}


	public DirectionCardRecycleview ()
	{
		super ();
		if (getClass () == DirectionCardRecycleview.class)
			mono.android.TypeManager.Activate ("BANKSAMPLEAPP.DirectionCardRecycleview, BANKSAMPLEAPP", "", this, new java.lang.Object[] {  });
	}

	public DirectionCardRecycleview (md5a7d95272a0c492154e5f7a7a7cd6fcd5.Result p0)
	{
		super ();
		if (getClass () == DirectionCardRecycleview.class)
			mono.android.TypeManager.Activate ("BANKSAMPLEAPP.DirectionCardRecycleview, BANKSAMPLEAPP", "BANKSAMPLEAPP.Model.Result, BANKSAMPLEAPP", this, new java.lang.Object[] { p0 });
	}


	public int getItemCount ()
	{
		return n_getItemCount ();
	}

	private native int n_getItemCount ();


	public void onBindViewHolder (android.support.v7.widget.RecyclerView.ViewHolder p0, int p1)
	{
		n_onBindViewHolder (p0, p1);
	}

	private native void n_onBindViewHolder (android.support.v7.widget.RecyclerView.ViewHolder p0, int p1);


	public android.support.v7.widget.RecyclerView.ViewHolder onCreateViewHolder (android.view.ViewGroup p0, int p1)
	{
		return n_onCreateViewHolder (p0, p1);
	}

	private native android.support.v7.widget.RecyclerView.ViewHolder n_onCreateViewHolder (android.view.ViewGroup p0, int p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
