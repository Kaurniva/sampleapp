package md5a7d95272a0c492154e5f7a7a7cd6fcd5;


public class GoogleDirectionResponse
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("BANKSAMPLEAPP.Model.GoogleDirectionResponse, BANKSAMPLEAPP", GoogleDirectionResponse.class, __md_methods);
	}


	public GoogleDirectionResponse ()
	{
		super ();
		if (getClass () == GoogleDirectionResponse.class)
			mono.android.TypeManager.Activate ("BANKSAMPLEAPP.Model.GoogleDirectionResponse, BANKSAMPLEAPP", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
