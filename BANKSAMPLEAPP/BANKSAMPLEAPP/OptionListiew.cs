﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BANKSAMPLEAPP.APIInterface;

namespace BANKSAMPLEAPP
{
    //[Activity(Label = "OptionListiew")]
    public class OptionListiew : DialogFragment

    {

        private string[] opt = { "BANK", "ATM" };
        private ListView lv;
        private ArrayAdapter ArrayAdapter;
        private Context mContext;
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.layoutFilter, container, false);

            //lv = v.FindViewById<ListView>(Resource.Id.listitemss);
            ArrayAdapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, opt);
            lv.Adapter = ArrayAdapter;
            lv.ItemClick += lv_ItemClick;

            return v;
        }
        public override void OnAttach(Context context)
        {

            mContext = context;
            base.OnAttach(context);
        }
        private void lv_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

            //Toast.MakeText(this.Activity, opt[e.Position], ToastLength.Short).Show();

            if (opt[e.Position].Equals("BANK"))
            {
                ((MainActivity)mContext).type("bank");
            }
            else if (opt[e.Position].Equals("ATM"))
            {
                ((MainActivity)mContext).type("atm");
            }


        }
    }
}
