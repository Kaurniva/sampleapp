﻿
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using System;
using Android.Locations;
using Android.Support.V4.App;
using Android.Util;
using System.Collections.Generic;
using System.Linq;
using Android.Content.PM;
using Android;
using Android.Views;
using String = System.String;
using BANKSAMPLEAPP.APIInterface;
using Result = BANKSAMPLEAPP.Model.Result;
using Location = BANKSAMPLEAPP.Model.Location;
using BANKSAMPLEAPP.Request;
using BANKSAMPLEAPP.Helper;
using System.Threading.Tasks;
using Android.Support.V7.Widget;
using Newtonsoft.Json;
using BANKSAMPLEAPP.Model;
using Android.Content;
using Android.Views.InputMethods;
using Plugin.Permissions;


//using Android.Gms.Maps IOnMapReadyCallback

namespace BANKSAMPLEAPP
{
    [Activity(Label = "@string/app_name")]
    public class MainActivity : AppCompatActivity, IOnMapReadyCallback, ILocationListener
    {
        public static readonly string TAG = "BankSampleApp";
        private static readonly string BANK_ATM = "bank|atm";
        private static readonly string BANK = "bank";
        private static readonly string ATM = "atm";
        private static readonly bool DEBUG = true;

        private LocationManager locationManager;
        private string locationProvider;
        private static readonly int REQUEST_LOCATION = 1;
        public GoogleMap GMap;
        private bool IsOnLocationUpdated;
        private const int RequestLocationId = 0;
        private GoogleAPIInterface googleAPIInterface;
        RecyclerView mRecycleView;
        ImageView searchview;
        TextView TextView;
        ImageView filer1;
        private EditText searchEditText;
        RecyclerView.LayoutManager mLayoutManager;
        private Android.App.FragmentManager fm;
        private OptionListiew opt;
        private Android.Locations.Location locationData;
        string iconType;
        View dividerLine;
        AtmDetails adapter;
        private List<Result> result = new List<Result>();
        private List<Result> resultFullData = new List<Result>();
        [Obsolete]
        private ProgressDialog progress;
        private Android.App.AlertDialog alert;
        Android.App.AlertDialog.Builder dialog;
        private InputMethodManager imm;

        // private SelectionSetting Cell;
        private readonly string[] PermissionsLocation =
        {
        Manifest.Permission.AccessCoarseLocation,
        Manifest.Permission.AccessFineLocation
        };
        private readonly String[] TYPES = { "Bank", "Atm" };

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            FragmentManager.FindFragmentById<MapFragment>(Resource.Id.googlemap).GetMapAsync(this);
            searchview = FindViewById<ImageView>(Resource.Id.image_search);
            filer1 = FindViewById<ImageView>(Resource.Id.image_filter);
            searchEditText = FindViewById<EditText>(Resource.Id.text_search);
            //dividerLine = FindViewById<View>(Resource.Id.divider);
            searchEditText.KeyPress += OnEditTextKeyPress;
            mRecycleView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Horizontal, false);
            mRecycleView.SetLayoutManager(mLayoutManager);
            adapter = new AtmDetails(result);

            mRecycleView.SetAdapter(adapter);
            filer1.Click += filer1_Click;
            fm = this.FragmentManager;
            //  opt = new OptionListiew();

            SetUpMap();

        }


        private void filer1_Click(object sender, EventArgs e)
        {
            ShowDialogBox();

        }

        private string GetSearchText()
        {
            return searchEditText.Text;
        }

        protected void OnEditTextKeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                var editText = (EditText)sender;
                Utility.GetInstance.DismissKeyboard(this, searchEditText);
                _ = FetchPlaceApiResultFromServerAsyncBasedSearchText(editText.Text);
            }

        }


        private void SetUpMap()
        {
            googleAPIInterface = GoogleAPIModel.GetInstance.GoogleAPIInterface;

            if (GMap == null)
            {
                IsOnLocationUpdated = false;

                if (HasLocationPermissionEnabled())
                {
                    InitializeLocationManager();
                }
                else
                {
                    ShowLocationPermissionRunTimePermissionDialog();
                }
            }


        }

        private GooglePlaceApiRequest CreateGooglePlaceApiRequestForLocation(double lat, double lng, string type)
        {
            GooglePlaceApiRequest googlePlaceApiRequest = new GooglePlaceApiRequest();
            googlePlaceApiRequest.key = Utility.GetInstance.GetGoogleApiMetaData(this);
            googlePlaceApiRequest.Location = lat + "," + lng;
            googlePlaceApiRequest.Radius = 5000;
            googlePlaceApiRequest.Sensor = true;
            googlePlaceApiRequest.Types = type;
            return googlePlaceApiRequest;
        }

        private GoogleFindPlaceFromTextRequest CreateGooglePlaceApiRequestForSearch(string search)
        {
            GoogleFindPlaceFromTextRequest googleFindPlaceFromTextRequest = new GoogleFindPlaceFromTextRequest();
            googleFindPlaceFromTextRequest.input = System.Net.WebUtility.UrlEncode(search);
            googleFindPlaceFromTextRequest.inputtype = Utility.TEXTQUERY;
            googleFindPlaceFromTextRequest.key = Utility.GetInstance.GetGoogleApiMetaData(this);
            googleFindPlaceFromTextRequest.fields = Utility.NAME + "," + Utility.FORMATTED_ADDRESS + "," + Utility.GEOMETRY + "," + Utility.TYPE;
            return googleFindPlaceFromTextRequest;
        }

        private async Task FetchPlaceApiResultFromServerAsyncBasedOnLocation(double lat, double lng, string paramType)
        {
            CreateProgressBar();
            GooglePlaceApiRequest googlePlaceApiRequest = CreateGooglePlaceApiRequestForLocation(lat, lng, paramType);
            await Task.Factory.StartNew(() =>
            {
                Task<string> googlePlaceResponse = googleAPIInterface.GetPlaceList(googlePlaceApiRequest);
                Log.Debug(TAG, "The google api output: " + googlePlaceResponse.Result);
                DismissProgressBar();

                Model.GooglePlaceResponse googlePlaceResponseObject = JsonConvert.DeserializeObject<GooglePlaceResponse>(googlePlaceResponse.Result);
                Log.Debug(TAG, "Google place api error message: " + (googlePlaceResponseObject.errorMessage != null && googlePlaceResponseObject.errorMessage.Length != 0));
                if (googlePlaceResponseObject.errorMessage != null && googlePlaceResponseObject.errorMessage.Length != 0)
                {
                    Utility.GetInstance.ShowToast(this, googlePlaceResponseObject.errorMessage);
                    return googlePlaceResponse;
                }
                List<Result> results = googlePlaceResponseObject.results;
                if (results != null)
                {
                    resultFullData.Clear();
                    resultFullData.AddRange(RemoveOtherTypePlaceItem(results));
                    List<Result> results1 = FindMaxThreeElementFromResultList(resultFullData);
                    Log.Debug(TAG, "Google place result not null?: " + (resultFullData != null) + " ,size: " + ((results1 != null) ? resultFullData.Count() : 0));
                    UpdateMapAndRecyclerUI(results1);
                }
                return googlePlaceResponse;
            });


        }

        private async Task FetchPlaceApiResultFromServerAsyncBasedOnLocation1(double lat, double lng, string paramType)
        {
            CreateProgressBar();
            GooglePlaceApiRequest googlePlaceApiRequest = CreateGooglePlaceApiRequestForLocation(lat, lng, paramType);
            await Task.Factory.StartNew(() =>
            {
                Task<string> googlePlaceResponse = googleAPIInterface.GetPlaceList(googlePlaceApiRequest);
                Log.Debug(TAG, "The google api output: " + googlePlaceResponse.Result);
                DismissProgressBar();
                Model.GooglePlaceResponse googlePlaceResponseObject = JsonConvert.DeserializeObject<GooglePlaceResponse>(googlePlaceResponse.Result);
                Log.Debug(TAG, "Google place api error message: " + (googlePlaceResponseObject.errorMessage != null && googlePlaceResponseObject.errorMessage.Length != 0));
                if (googlePlaceResponseObject.errorMessage != null && googlePlaceResponseObject.errorMessage.Length != 0)
                {
                    Utility.GetInstance.ShowToast(this, googlePlaceResponseObject.errorMessage);
                    return googlePlaceResponse;
                }
                List<Result> results = googlePlaceResponseObject.results;

                List<Result> resultCount = new List<Result>();
                if (results != null)
                {
                    resultFullData.AddRange(RemoveOtherTypePlaceItem(results));
                    List<Result> results1 = FindMaxThreeElementFromResultList(resultFullData);
                    Log.Debug(TAG, "Google place result not null?: " + (resultFullData != null) + " ,size: " + ((resultFullData != null) ? results.Count() : 0));
                    UpdateMapAndRecyclerUI(results1);
                }
                return googlePlaceResponse;
            });
        }

        private List<Result> FindMaxThreeElementFromResultList(List<Result> results)
        {
            if (results != null && results.Count() > 0)
            {
                List<Result> result = new List<Result>();
                if (results.Count() > 3)
                {
                    for (int index = 0; index < 3; index++)
                    {
                        result.Add(results[index]);
                    }
                    return result;
                }
            }
            return results;
        }

        private List<Result> FindResultElementByType(string type)
        {
            if (resultFullData.Count() > 0)
            {
                List<Result> results = new List<Result>();
                for (int index = 0; index < resultFullData.Count(); index++)
                {
                    Result result = resultFullData[index];
                    string typeOfResult = GetBankMarkerType(result.typesList);
                    if (typeOfResult == null || typeOfResult.Length == 0)
                    {
                        continue;
                    }
                    else if (typeOfResult.Equals(type, StringComparison.OrdinalIgnoreCase))
                    {
                        results.Add(result);
                    }
                }
                return results;
            }
            return null;
        }


        private List<Result> RemoveOtherTypePlaceItem(List<Result> results)
        {
            List<Result> items = new List<Result>();
            foreach (Result resul in results)
            {
                string type = GetBankMarkerType(resul.typesList);
                if (type != null)
                {
                    items.Add(resul);
                }
            }
            return items;
        }

        private async Task FetchPlaceApiResultFromServerAsyncBasedOnLocation(Android.Locations.Location location, string paramType)
        {

            FetchPlaceApiResultFromServerAsyncBasedOnLocation(location.Latitude, location.Longitude, paramType);
        }


        private async Task FetchPlaceApiResultFromServerAsyncBasedSearchText(string search)
        {

            CreateProgressBar();
            GoogleFindPlaceFromTextRequest googlePlaceApiRequest = CreateGooglePlaceApiRequestForSearch(search);
            await Task.Factory.StartNew(() =>
            {
                Task<string> googlePlaceResponse = googleAPIInterface.GetPlaceListFromText(googlePlaceApiRequest);
                Log.Debug(TAG, "The google api output: " + googlePlaceResponse.Result);
                DismissProgressBar();
                Model.GoogleApiSearchResponse googlePlaceResponseObject = JsonConvert.DeserializeObject<GoogleApiSearchResponse>(googlePlaceResponse.Result);
                Log.Debug(TAG, "Google place api error message: " + (googlePlaceResponseObject.errorMessage != null && googlePlaceResponseObject.errorMessage.Length != 0));
                if (googlePlaceResponseObject.errorMessage != null && googlePlaceResponseObject.errorMessage.Length != 0)
                {
                    Utility.GetInstance.ShowToast(this, googlePlaceResponseObject.errorMessage);
                    return googlePlaceResponse;
                }
                List<CandidatesItem> candidatesItem = googlePlaceResponseObject.candidatesItems;
                if (candidatesItem != null && candidatesItem.Count > 0)
                {
                    CandidatesItem candidates = candidatesItem[0];
                    RunOnUiThread(() =>
                    {
                        _ = FetchPlaceApiResultFromServerAsyncBasedOnLocation1(candidates.geometry.location.lat, candidates.geometry.location.lng, BANK_ATM);
                    });

                }
                return googlePlaceResponse;
            });
        }

        private void UpdateMapAndRecyclerUI(List<Result> results)
        {
            RunOnUiThread(() =>
            {
                if (results != null)
                {
                    /*
                     * It will create marker in map UI before that loop clear all marker in map. 
                     */
                    Log.Debug(TAG, " Google map not null: " + GMap);
                    if (GMap != null)
                    {
                        GMap.Clear();
                        foreach (Result item in results)
                        {
                            Location locationObject = item.geometry.location;
                            Console.WriteLine("The data output: lat " + locationObject.lat);
                            Console.WriteLine("The data output: lng " + locationObject.lng);
                            string type = GetBankMarkerType(item.typesList);
                            if (type == null || type.Length == 0)
                            {
                                continue;
                            }
                            else
                            {
                                item.isBankItem = type.Equals(BANK, StringComparison.OrdinalIgnoreCase);
                            }
                            ShowLocationMarker(locationObject.lat, locationObject.lng, type);
                        }
                    }

                    /*
                     * It will update bottom recycler view
                     */
                    result.Clear();
                    result.AddRange(results);
                    Log.Debug(TAG, " Recycler adapter object not null: " + (adapter != null) + " ,list size: " + result.Count());
                    adapter.NotifyDataSetChanged();
                }
            });
        }

        private void UpdateMapAndRecyclerUI()
        {
            RunOnUiThread(() =>
            {
                if (result != null && result.Count() > 0)
                {
                    /*
                     * It will create marker in map UI before that loop clear all marker in map. 
                     */
                    Log.Debug(TAG, " Google map not null: " + GMap);
                    if (GMap != null)
                    {
                        GMap.Clear();
                        foreach (Result item in result)
                        {
                            Location locationObject = item.geometry.location;
                            string type = GetBankMarkerType(item.typesList);
                            if (type == null || type.Length == 0)
                            {
                                continue;
                            }
                            else
                            {
                                item.isBankItem = type.Equals(BANK, StringComparison.OrdinalIgnoreCase);
                            }
                            ShowLocationMarker(locationObject.lat, locationObject.lng, type);
                        }
                    }
                    /*
                     * It will update bottom recycler view
                     */
                    Log.Debug(TAG, " Recycler adapter object not null: " + (adapter != null) + " ,list size: " + result.Count());
                    adapter.NotifyDataSetChanged();
                }
            });
        }

        private string GetBankMarkerType(List<string> list)
        {
            if (list == null)
                return null;
            foreach (string s in list)
            {
                if (s.Equals(BANK, StringComparison.OrdinalIgnoreCase))
                {
                    return s;
                }
                else if (s.Equals(ATM, StringComparison.OrdinalIgnoreCase))
                {
                    return s;
                }
            }
            return null;
        }


        private bool HasLocationPermissionEnabled()
        {

            foreach (string element in PermissionsLocation)
            {
                if (CheckSelfPermission(element) != (int)Permission.Granted)
                {
                    return false;
                }
            }
            return true;

        }


        private void ShowLocationPermissionRunTimePermissionDialog()
        {
            //Finally request permissions with the list of permissions and Id
            RequestPermissions(PermissionsLocation, REQUEST_LOCATION);
        }

        private void InitializeLocationManager()
        {
            locationManager = (LocationManager)GetSystemService(LocationService);
            Criteria criteriaForLocationService = new Criteria
            {
                Accuracy = Accuracy.Coarse
            };
            IList<string> acceptableLocationProviders = locationManager.GetProviders(criteriaForLocationService, true);
            if (acceptableLocationProviders.Any())
            {
                locationProvider = acceptableLocationProviders.First();
            }
            else
            {
                locationProvider = string.Empty;
            }
            Log.Debug(TAG, "Using " + locationProvider + ".");
        }

        private void ListClicked(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (alert != null)
            {
                alert.Dismiss();
            }
            int position = e.Position;
            iconType = TYPES[position];
            List<Result> results = FindResultElementByType(iconType);
            UpdateMapAndRecyclerUI(results);
        }


        private void ShowDialogBox()
        {
            // requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog = new Android.App.AlertDialog.Builder(this);
            alert = dialog.Create();
            ListView lv = new ListView(this);
            lv.DividerHeight = 0;
            lv.Adapter = new ArrayAdapter(this, Resource.Layout.layoutFilter, Android.Resource.Id.Text1, TYPES);
            lv.ItemClick += ListClicked;
            alert.SetView(lv);
            alert.SetButton("CANCEL", (c, ev) =>
            {

            });
            alert.Show();
        }
     
        protected override void OnResume()
        {

            base.OnResume();
            UpdateLocationManager();

        }

        protected override void OnPause()
        {

            base.OnPause();
            RemoveLocationManagerUpdate();
        }

        public override void OnBackPressed()
        {
            Toast.MakeText(this, "OnBackkkkk detected", ToastLength.Long).Show();
            base.OnBackPressed();
            RemoveLocationManagerUpdate();
        }

        private void DismissProgressBar()
        {
            if (progress != null && progress.IsShowing)
            {
                progress.Dismiss();
                progress = null;
            }
        }
        private void CreateProgressBar()
        {
            if (progress == null)
            {
                progress = new ProgressDialog(this);
                progress.Indeterminate = true;
                progress.SetProgressStyle(Android.App.ProgressDialogStyle.Spinner);
                progress.SetMessage("Loading is Progress...");
                progress.SetCancelable(false);
                progress.Show();
            }
            else
            {
                progress.Show();
            }
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            this.GMap = googleMap;
            GMap.UiSettings.ZoomControlsEnabled = true;
            CreateProgressBar();
            Handler handler = new Handler();
            handler.PostDelayed(() =>
            {
                //Wait 2 mins to fetch current location
                DismissProgressBar();
                if (!IsOnLocationUpdated)
                {
                    if (Utility.GetInstance.GetLastKnownLocationDetails(this) != null)
                    {
                        IsOnLocationUpdated = true;
                        CreateProgressBar();
                        double lat = Double.Parse(Utility.GetInstance.GetLastKnownLocationLat(this));
                        double lng = Double.Parse(Utility.GetInstance.GetLastKnownLocationLng(this));
                        _ = FetchPlaceApiResultFromServerAsyncBasedOnLocation(lat, lng, BANK_ATM);
                    }
                    else
                    {
                        Log.Warn(TAG, " Unable to Find User Location");
                        Utility.GetInstance.ShowToast(this, "Unable to find Location");
                    }
                }
            }, 2000);

        }

        private void RemoveLocationManagerUpdate()
        {

            if (locationManager != null)
            {
                locationManager.RemoveUpdates(this);
            }
        }

        private void UpdateLocationManager()
        {
            if (locationManager != null)
            {
                locationManager.RequestLocationUpdates(locationProvider, 0, 0, this);
            }
        }

        protected override void OnRestart()
        {
            base.OnRestart();
            UpdateMapAndRecyclerUI();
        }

        protected override void OnStop()
        {
            base.OnStop();
            IsOnLocationUpdated = false;
        }

        public void ShowLocationMarker(double lat, double lng, string iconType)
        {
            Log.Debug(TAG, " Create marker called: ");
            LatLng latlng = new LatLng(lat, lng);
            CameraUpdate camera = CameraUpdateFactory.NewLatLngZoom(latlng, 15);
            GMap.MoveCamera(camera);
            if (iconType != null)
            {
                if (iconType.Equals(BANK, StringComparison.OrdinalIgnoreCase))
                {
                    MarkerOptions options = new MarkerOptions().SetPosition(latlng).SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.bank_icn));
                    GMap.AddMarker(options);
                    RemoveLocationManagerUpdate();
                }
                else if (iconType.Equals(ATM, StringComparison.OrdinalIgnoreCase))
                {
                    MarkerOptions options = new MarkerOptions().SetPosition(latlng).SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.atm_icn));
                    GMap.AddMarker(options);
                    RemoveLocationManagerUpdate();
                }
            }
            else
            {
                MarkerOptions options = new MarkerOptions().SetPosition(latlng).SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.bank_icn));
                GMap.AddMarker(options);
                RemoveLocationManagerUpdate();
            }
        }


        public void OnLocationChanged(Android.Locations.Location location)
        {
            if (location != null && !IsOnLocationUpdated)
            {
                Utility.GetInstance.SaveLastLocationLat(this, location.Latitude);
                Utility.GetInstance.SaveLastLocationLng(this, location.Longitude);
                Utility.GetInstance.currentLocation = location;
                Log.Debug(TAG, " Current location Lat: " + location.Latitude + " ,Longitude: " + location.Longitude);
                IsOnLocationUpdated = true;
                locationData = location;
                CreateProgressBar();
                _ = FetchPlaceApiResultFromServerAsyncBasedOnLocation(location, BANK_ATM);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }



        public void OnProviderDisabled(string provider)
        {

        }

        public void OnProviderEnabled(string provider)
        {

        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {

        }

        public void type(string type)
        {

            CreateProgressBar();
            _ = FetchPlaceApiResultFromServerAsyncBasedOnLocation(locationData, type);
        }
    }

    public interface OnItemClick
    {
        void ItemClick(int position);
    }
}
