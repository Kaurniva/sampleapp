﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using BANKSAMPLEAPP.Model;
using Newtonsoft.Json;
using Result = BANKSAMPLEAPP.Model.Result;

namespace BANKSAMPLEAPP
{
    [Activity(Label = "AtmDetails")]


    public class RecycleViewHolder : RecyclerView.ViewHolder
    {
        public ImageView atmImage { get; set; }
        public TextView name { get; set; }
        public TextView address { get; set; }

        public TextView distance { get; set; }
        public ImageView direction { get; set; }

        public TextView directionText { get; set; }

        private View view;

        public RecycleViewHolder(View itemview) : base(itemview)
        {
            view = itemview;
            atmImage = itemview.FindViewById<ImageView>(Resource.Id.atmImage1);
            name = itemview.FindViewById<TextView>(Resource.Id.bankname);
            address = itemview.FindViewById<TextView>(Resource.Id.bankAddress);
            distance = itemview.FindViewById<TextView>(Resource.Id.bankDistance);
            direction = itemview.FindViewById<ImageView>(Resource.Id.bankDirection);
            directionText = itemview.FindViewById<TextView>(Resource.Id.totalDistance);

            direction.Visibility = ViewStates.Visible;
            directionText.Visibility = ViewStates.Visible;

        }




    }


    public class AtmDetails : RecyclerView.Adapter
    {


        public List<Result> result = new List<Result>();
        public OnItemClick OnItem;

        public AtmDetails(List<Result> result)
        {
            this.result = result;

        }

        private void ShowDireactionScreen(Context context, Result result)
        {
            Intent NxtAct = new Intent(context, typeof(DirectionActivity));
            //Bundle bundle = new Bundle();
            //bundle.PutSerializable(Helper.Utility.RESULT_ARG, result);
            NxtAct.PutExtra(Helper.Utility.RESULT_ARG, JsonConvert.SerializeObject(result));
            context.StartActivity(NxtAct);
        }

        public void SetOnItemClick(OnItemClick itemClick)
        {
            this.OnItem = itemClick;
        }

        public override int ItemCount
        {
            get { return result.Count(); }
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            RecycleViewHolder vh = holder as RecycleViewHolder;
            Result resultValue = result[position];
            vh.name.Text = resultValue.name;
            vh.address.Text = resultValue.vicinity;


            if (resultValue.isBankItem)
            {
                vh.atmImage.SetBackgroundResource(Resource.Drawable.bank_icn);
            }
            else
            {
                vh.atmImage.SetBackgroundResource(Resource.Drawable.atm_icn);
            }
            vh.direction.Click += delegate {
                ShowDireactionScreen(vh.direction.Context, resultValue);
            };

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.atm_card_details, parent, false);
            return new RecycleViewHolder(itemView);

        }
    }

}