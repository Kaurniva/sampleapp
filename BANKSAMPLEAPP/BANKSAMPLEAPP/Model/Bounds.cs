﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
   
    public class Bounds 
    {
        [JsonProperty(PropertyName = "northeast")]
        public Northeast northeast { get; set; }

        [JsonProperty(PropertyName = "southwest")]
        public Southwest southwest { get; set; }
    }
}