﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
    public class CandidatesItem
    {
        [JsonProperty(PropertyName = "geometry", NullValueHandling = NullValueHandling.Ignore)]
        public Geometry geometry { get; set; }
        [JsonProperty(PropertyName = "formatted_address", NullValueHandling = NullValueHandling.Ignore)]
        public string formattedAddress { get; set; }
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }
    }
}