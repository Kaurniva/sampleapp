﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;


namespace BANKSAMPLEAPP.Model
{

    public class Route
    {
        internal string totalDistance;

        [JsonProperty(PropertyName = "bounds")]
        public Bounds bounds { get; set; }

        [JsonProperty(PropertyName = "copyrights")]
        public string copyrights { get; set; }

        [JsonProperty(PropertyName = "legs")]
        public List<Leg> legs { get; set; }

        [JsonProperty(PropertyName = "overview_polyline")]
        public OverviewPolyline overview_polyline { get; set; }

        [JsonProperty(PropertyName = "summary")]
        public string summary { get; set; }

        [JsonProperty(PropertyName = "warnings")]
        public List<object> warnings { get; set; }

        [JsonProperty(PropertyName = "waypoint_order")]
        public List<object> waypoint_order { get; set; }

        public List<string> typesList { get; set; }
    }
}