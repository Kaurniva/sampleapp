﻿using System;
using Newtonsoft.Json;
namespace BANKSAMPLEAPP.Model
{ 
 public class Location
{
    [JsonProperty(PropertyName = "lat")]
    public double lat { get; set; }
    [JsonProperty(PropertyName = "lng")]
    public double lng { get; set; }
}
}

