﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
    public class GoogleDirectionResponse : Java.Lang.Object
    {
        public static object Route { get; internal set; }

        [DefaultValue("")]
        [JsonProperty(PropertyName = "error_message", NullValueHandling = NullValueHandling.Ignore)]
        public string errorMessage { get; set; }
        [DefaultValue("")]
        [JsonProperty(PropertyName = "status", NullValueHandling = NullValueHandling.Ignore)]
        public string status { get; set; }
        [JsonProperty(PropertyName = "routes", NullValueHandling = NullValueHandling.Ignore)]
        public List<Route> routes { get; set; }

    }
}