﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{


    public class GeocodedWaypoints
    {
        [JsonProperty(PropertyName = "geocoder_status")]
        public string geocoder_status { get; set; }

        [JsonProperty(PropertyName = "place_id")]
        public string place_id { get; set; }

        [JsonProperty(PropertyName = "types")]
        public List<string> types { get; set; }
    }
}