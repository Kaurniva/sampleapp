﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
   
    public class EndLocation2
    {
        [JsonProperty(PropertyName = "lat")]
        public double lat { get; set; }
        [JsonProperty(PropertyName = "lng")]
        public double lng { get; set; }
    }
}