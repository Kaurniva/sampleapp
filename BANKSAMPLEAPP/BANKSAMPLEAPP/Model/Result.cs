﻿using System;
using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using BANKSAMPLEAPP.Helper;
using Java.Interop;
using Newtonsoft.Json;
namespace BANKSAMPLEAPP.Model
{
    public class Result : Java.Lang.Object, Java.IO.ISerializable
    {
        [JsonProperty(PropertyName = "geometry", NullValueHandling = NullValueHandling.Ignore)]
        public Geometry geometry { get; set; }

        [JsonProperty(PropertyName = "icon", NullValueHandling = NullValueHandling.Ignore)]
        public string icon { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        // public OpeningHours opening_hours { get; set; }
        // public List<Photo> photos { get; set; }
        [JsonProperty(PropertyName = "place_id", NullValueHandling = NullValueHandling.Ignore)]
        public string place_id { get; set; }
        //public PlusCode plus_code { get; set; }
        [JsonProperty(PropertyName = "rating", NullValueHandling = NullValueHandling.Ignore)]
        public double rating { get; set; }
        [JsonProperty(PropertyName = "reference", NullValueHandling = NullValueHandling.Ignore)]
        public string reference { get; set; }
        [JsonProperty(PropertyName = "scope", NullValueHandling = NullValueHandling.Ignore)]
        public string scope { get; set; }
        //public List<string> types { get; set; }
        // public int user_ratings_total { get; set; }
        [JsonProperty(PropertyName = "vicinity", NullValueHandling = NullValueHandling.Ignore)]
        public string vicinity { get; set; }
        [JsonProperty(PropertyName = "types", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> typesList { get; set; }

        public bool isBankItem { get; set; }

        public Result()
        {

        }
    }
}
