﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{

    public class Geometry
    {
        [JsonProperty(PropertyName = "location", NullValueHandling = NullValueHandling.Ignore)]
        public Location location { get; set; }
        //public Viewport viewport { get; set; }
    }
}
