﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
    public class GoogleApiSearchResponse
    {
        [JsonProperty(PropertyName = "candidates", NullValueHandling = NullValueHandling.Ignore)]
        public List<CandidatesItem> candidatesItems { get; set; }

        [JsonProperty(PropertyName = "status", NullValueHandling = NullValueHandling.Ignore)]
        public string status { get; set; }

        [DefaultValue("")]
        [JsonProperty(PropertyName = "error_message", NullValueHandling = NullValueHandling.Ignore)]
        public string errorMessage { get; set; }
    }
}