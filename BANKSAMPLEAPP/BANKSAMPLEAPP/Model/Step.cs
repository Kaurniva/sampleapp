﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
    
    public class Step
    {
        [JsonProperty(PropertyName = "distance")]
        public Distance2 distance { get; set; }

        [JsonProperty(PropertyName = "duration")]
        public Duration2 duration { get; set; }

        [JsonProperty(PropertyName = "end_location")]
        public EndLocation2 end_location { get; set; }

        [JsonProperty(PropertyName = "html_instructions")]
        public string html_instructions { get; set; }

        [JsonProperty(PropertyName = "polyline")]
        public Polyline polyline { get; set; }

        [JsonProperty(PropertyName = "start_location")]
        public StartLocation2 start_location { get; set; }

        [JsonProperty(PropertyName = "travel_mode")]
        public string travel_mode { get; set; }

        [JsonProperty(PropertyName = "maneuver")]
        public string maneuver { get; set; }
    }
}