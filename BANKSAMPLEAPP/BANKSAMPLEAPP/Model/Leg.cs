﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;


namespace BANKSAMPLEAPP.Model
{
  
    public class Leg 
    {
        [JsonProperty(PropertyName = "distance")]
        public Distance distance { get; set; }

        [JsonProperty(PropertyName = "duration")]
         public Duration duration { get; set; }

        [JsonProperty(PropertyName = "end_address")]
        public string end_address { get; set; }

        [JsonProperty(PropertyName = "end_location")]
        public EndLocation end_location { get; set; }

        [JsonProperty(PropertyName = "start_address")]
        public string start_address { get; set; }

        [JsonProperty(PropertyName = "start_location")]
        public StartLocation start_location { get; set; }

      [JsonProperty(PropertyName = "steps")]
      public List<Step> steps { get; set; }

        [JsonProperty(PropertyName = "traffic_speed_entry")]
        public List<object> traffic_speed_entry { get; set; }

        [JsonProperty(PropertyName = "via_waypoint")]
        public List<object> via_waypoint { get; set; }
    }
}