﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BANKSAMPLEAPP.Model
{
    public class GooglePlaceResponse
    {
        public static object Result { get; internal set; }
        [DefaultValue("")]
        [JsonProperty(PropertyName = "error_message", NullValueHandling = NullValueHandling.Ignore)]
        public string errorMessage { get; set; }
        [DefaultValue("")]
        [JsonProperty(PropertyName = "status", NullValueHandling = NullValueHandling.Ignore)]
        public string status { get; set; }
        [JsonProperty(PropertyName = "results", NullValueHandling = NullValueHandling.Ignore)]
        public List<Result> results { get; set; }
    }
}
