﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Refit;

namespace BANKSAMPLEAPP.Request
{

    public class GoogleApiDirectionRequest
    {
        [AliasAs("origin")]
        public string origin { get; set; }

        [AliasAs("key")]
        public string key { get; set; }

        [AliasAs("destination")]
        public string destination { get; set; }

        [AliasAs("mode")]
        public string mode { get; set; }

    }
}