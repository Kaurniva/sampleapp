﻿using System;
using Refit;

namespace BANKSAMPLEAPP.Request
{
    public class GooglePlaceApiRequest
    {
        [AliasAs("location")]
        public string Location { get; set; }

        [AliasAs("radius")]
        public int Radius { get; set; }

        [AliasAs("types")]
        public string Types { get; set; }

        [AliasAs("sensor")]
        public bool Sensor { get; set; }

        [AliasAs("key")]
        public string key { get; set; }
    }
}
