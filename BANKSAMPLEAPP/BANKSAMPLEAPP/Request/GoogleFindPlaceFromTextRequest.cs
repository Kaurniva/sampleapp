﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Refit;

namespace BANKSAMPLEAPP.Request
{
    public class GoogleFindPlaceFromTextRequest
    {
        [AliasAs("input")]
        public string input { get; set; }

        [AliasAs("inputtype")]
        public string inputtype { get; set; }

        [AliasAs("fields")]
        public string fields { get; set; }

        [AliasAs("key")]
        public string key { get; set; }

    }
}