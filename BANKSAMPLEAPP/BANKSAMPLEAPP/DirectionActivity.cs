﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using BANKSAMPLEAPP.Model;
using BANKSAMPLEAPP.APIInterface;
using Route = BANKSAMPLEAPP.Model.Route;
using Location = BANKSAMPLEAPP.Model.Location;
using BANKSAMPLEAPP.Request;
using BANKSAMPLEAPP.Helper;
using System.Threading.Tasks;
using Android.Support.V7.Widget;
using Android.Views.InputMethods;
using Newtonsoft.Json;

using Android.Content.PM;
using Android;
using Android.Util;
using Result = BANKSAMPLEAPP.Model.Result;

namespace BANKSAMPLEAPP
{
    [Activity(Label = "DirectionActivity", Theme = "@style/ActionBar")]
    public class DirectionActivity : AppCompatActivity
    {
        public static readonly string TAG = "BankSampleApp";
        private static readonly string VIA = "via:-";
        private static readonly string COMMA = ",";
        //private static readonly string VIA_SEPARETE = "|"+VIA;

        private GoogleAPIInterface googleAPIInterface;
        private ProgressDialog progress;
        private InputMethodManager imm;
        private List<Leg> legs = new List<Leg>();
        private List<Step> steps = new List<Step>();
        private List<Route> route = new List<Route>();
        // private List<Result> result1 = new List<Result>();
        private GoogleApiDirectionRequest directionRequest;
        //private DirectionApiInterfaces directionApiInterface;
        RecyclerView directionRecycleVierw, mRecycleView;
        RecyclerView.LayoutManager mLayoutManager, mLayoutManager1;
        DirectionRecycleview adapter;
        private static readonly string BANK = "bank";
        private static readonly string ATM = "atm";
        string place_id;
        string destination;
        double geomatry;
        // Location Geometry.location { get; set; }
        private Android.Locations.Location locationData;
        // RecyclerView mRecycleView;
        // DirectionCardRecycleview adapter1;
        // RecyclerView.LayoutManager mLayoutManager1;
        string name;
        AtmDetails adapter1;
        private List<Result> result1 = new List<Result>();
        private TextView placeNameTextView;
        private TextView placeAddressTextView;
        private TextView placePinDetailsTextView;

        /*public int AIzaSyCxaNveHJWjXCGFsDOHrM0uj5ww9Nc_uEI { get; private set; }*/

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_direction);
            googleAPIInterface = GoogleAPIModel.GetInstance.GoogleAPIInterface;

            placeNameTextView = FindViewById<TextView>(Resource.Id.placeNameTextView);
            placeAddressTextView = FindViewById<TextView>(Resource.Id.place_address_text_view);
            placePinDetailsTextView = FindViewById<TextView>(Resource.Id.place_pin_details_text_view);

            directionRecycleVierw = FindViewById<RecyclerView>(Resource.Id.recycledirection);
            mLayoutManager = new LinearLayoutManager(this);
            directionRecycleVierw.SetLayoutManager(mLayoutManager);
            adapter = new DirectionRecycleview(steps);
            directionRecycleVierw.SetAdapter(adapter);
            directionRecycleVierw.AddItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.Vertical));

            string resultString = Intent.GetStringExtra(Utility.RESULT_ARG);
            if (resultString != null && resultString.Length > 0)
            {
                UpdateLocalVariableValues(resultString);
            }
            Log.Debug(TAG, " Selected place id: " + place_id + " ,Name: " + name);
            //
        }

        /**
         * This method will update place details result to local variables.
         * Convert the place details intent string to result object.
         */
        private void UpdateLocalVariableValues(string intentString)
        {
            Result result = JsonConvert.DeserializeObject<Result>(intentString);
            place_id = result.place_id;
            name = result.name;
            if (result != null)
            {
                //Log.Debug(TAG, " Location param: " + MakeLocationParam(result));
                UpdateAddressDetailsUI(result);
                if (PlacesDataHandler.GetInstance().IsPlaceDirectionDetailsAvailable(result.place_id))
                {
                    GoogleDirectionResponse googleDirectionResponse = PlacesDataHandler.GetInstance().GetPlaceDirectionDetails(result.place_id);
                    UpdateRoutRecyclerViewUI(googleDirectionResponse.routes);
                }
                else
                {
                    _ = FetchDirectionApiResultFromServerAsync(result);
                }

            }
            Log.Debug(TAG, " The intent value: " + result);
        }

        private void UpdateAddressDetailsUI(Result result)
        {
            placeNameTextView.Text = result.name;
            placeAddressTextView.Text = result.vicinity;
            //placePinDetailsTextView = FindViewById<TextView>(Resource.Id.place_pin_details_text_view);
        }

        /**
         * This method will fetch data from google server and update the route recyclerview.
         */
        private async Task FetchDirectionApiResultFromServerAsync(Result result)
        {
            CreateProgressBar();
            GoogleApiDirectionRequest googleApiDirectionRequest = CreateGoogleDirectionRequest(result);
            if (googleApiDirectionRequest == null)
            {
                DismissProgressBar();
                return;
            }
            await Task.Factory.StartNew(() =>
            {
                //Task<Route> googleApiDirectionResponse = googleAPIInterface.GetDirectionList(googleApiDirectionRequest);
                Task<string> googleApiDirectionResponse = googleAPIInterface.GetDirectionList(googleApiDirectionRequest);
                Log.Debug(TAG, "The google api output: " + googleApiDirectionResponse.Result);
                DismissProgressBar();
                Model.GoogleDirectionResponse googleDirectionResponseObject = JsonConvert.DeserializeObject<GoogleDirectionResponse>(googleApiDirectionResponse.Result);

                if (googleDirectionResponseObject.errorMessage != null && googleDirectionResponseObject.errorMessage.Length != 0)
                {
                    Utility.GetInstance.ShowToast(this, googleDirectionResponseObject.errorMessage);
                    return googleApiDirectionResponse;
                }
                PlacesDataHandler.GetInstance().UpdatePlaceDirectionDetails(result.place_id, googleDirectionResponseObject);
                UpdateRoutRecyclerViewUI(googleDirectionResponseObject.routes);
                return googleApiDirectionResponse;
            });
        }

        /**
         *This method will update the route recyclerview. 
         */
        private void UpdateRoutRecyclerViewUI(List<Route> routes)
        {
            if (routes == null)
                return;
            RunOnUiThread(() => {
                legs.Clear();
                route.Clear();
                steps.Clear();
                route.AddRange(routes);
                foreach (Route r in routes)
                {
                    legs.AddRange(r.legs);
                }
                foreach (Leg leg in legs)
                {
                    steps.AddRange(leg.steps);
                }
                /**
                 * Update action bar content.
                 */
                if (legs.Count() > 0)
                {
                    Leg leg = legs[0];
                    string titleDistance = leg.distance.text;
                    string subTitleDuration = leg.duration.text;
                    UpdateActionBarUI(titleDistance, subTitleDuration);
                }
                adapter.NotifyDataSetChanged();
            });

        }

        private void UpdateActionBarUI(string title, string subTitle)
        {
            var actionBar = this.SupportActionBar;
            actionBar.Title = title;
            actionBar.Subtitle = subTitle;
        }

        public GoogleApiDirectionRequest CreateGoogleDirectionRequest(Result result)
        {
            string placeId = Utility.PLACE_ID_REQUEST + result.place_id; ;
            Android.Locations.Location location = Utility.GetInstance.currentLocation;
            GoogleApiDirectionRequest googleApiDirectionRequest = new GoogleApiDirectionRequest();
            if (location != null)
            {
                googleApiDirectionRequest.origin = location.Latitude + COMMA + location.Longitude;
            }
            else if (Utility.GetInstance.GetLastKnownLocationDetails(this) != null)
            {
                googleApiDirectionRequest.origin = Utility.GetInstance.GetLastKnownLocationDetails(this);
            }
            else
            {
                Utility.GetInstance.ShowToast(this, "Unable to Find User location");
                return null;
            }
            googleApiDirectionRequest.key = "AIzaSyAfh-du45UuW_5rB56220ipNTWiduDE_ms";//Utility.GetInstance.GetGoogleApiMetaData(this);
            googleApiDirectionRequest.destination = placeId;
            googleApiDirectionRequest.mode = Utility.WALKING_KEY;
            Log.Debug(TAG, "The GoogleApiDirectionRequest: " + googleApiDirectionRequest);
            return googleApiDirectionRequest;
        }

        protected override void OnResume()
        {
            base.OnResume();

        }

        protected override void OnPause()
        {
            base.OnPause();

        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);

            StartActivity(typeof(MainActivity));
            // RemoveLocationManagerUpdate();
        }


        private void DismissProgressBar()
        {
            if (progress != null && progress.IsShowing)
            {
                progress.Dismiss();
                progress = null;
            }
        }
        private void CreateProgressBar()
        {
            if (progress == null)
            {
                progress = new Android.App.ProgressDialog(this);
                progress.Indeterminate = true;
                progress.SetProgressStyle(Android.App.ProgressDialogStyle.Spinner);
                progress.SetMessage("Loading is Progress...");
                progress.SetCancelable(false);
                progress.Show();
            }
            else
            {
                progress.Show();
            }
        }

    }
}