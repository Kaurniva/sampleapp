/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import App1 from './App1';
import Login from './Login'
import navigate from './navigate'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => navigate);
