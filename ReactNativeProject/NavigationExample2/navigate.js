import {createStackNavigator} from 'react-navigation-stack'
import App from './App'
import App1 from './App1'
import Login from './Login'
import {createAppContainer} from 'react-navigation'
import { logicalExpression } from '@babel/types';

const Stack = createStackNavigator({
    Home:{
        screen:App,
    },
    Other:{
        screen:App1,
    },
    Loogin:{
        screen:Login,
    }
},{
    initialRouteName:'Home',
    defaultNavigationOptions:{
        headerStyle:{
            backgroundColor:'#445'
        }
    }
});
const Navigate = createAppContainer(Stack);
export default Navigate;
