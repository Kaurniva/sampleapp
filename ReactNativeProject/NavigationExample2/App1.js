import React, {Component} from 'react';
import {View, Text, TouchableOpacity, TextInput, StyleSheet} from 'react-native';

  class App1 extends Component{

    state = {
        fname:'',
        lname:'',
        email: '',
        password: ''
     }
     handleFname = (text) => {
        this.setState({ fname: text })
     }
     handleLname = (text) => {
        this.setState({ lname: text })
     }

     handleEmail = (text) => {
        this.setState({ email: text })
     }
     handlePassword = (text) => {
        this.setState({ password: text })
     }
     login = (email, pass) => {
        alert('email: ' + email + ' password: ' + pass)
     }
     render() {
        return (
           <View style = {styles.container}>

<TextInput style = {styles.input}

                 underlineColorAndroid = "transparent"
                 placeholder = "FirstName"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handleFname}/>

                 <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "LastName"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handleLname}/>

                <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "DD/MM/YY"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handleLname}/>

                <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "Address"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handleLname}/>

              <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "Email"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handleEmail}/>
              
              <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "Password"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handlePassword}/>
              
              <TouchableOpacity
                 style = {styles.submitButton}
                 onPress = {
                    () => this.login(this.state.email, this.state.password)
                 }>
                 <Text style = {styles.submitButtonText}> Submit </Text>
              </TouchableOpacity>
           </View>
        )
     }
  }

  export default App1

  const styles = StyleSheet.create({
     container: {
         
        paddingTop: 23
     },
     input: {
        margin: 20,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1
     },
     submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        height: 40,
     },
     submitButtonText:{
        color: 'white'
     }
  })